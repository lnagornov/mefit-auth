<div align="center">
    <h1>Experis Academy Case: MeFit authentication service</h1>
    <img src="https://i.ibb.co/NL39Fv1/0-U7-ABnm-Lf-BHy-FKzk.png" width="128" alt="Keycloak-logo">
</div>

[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## MeFit auth

Keycloak authentication service handles login and profile manipulations for MeFit app users.

This repo is made for deploying Keycloak to Heroku, so that it can be used on the public internet. Other deployment options such as this exist, however Heroku has a generous free tier that does not expire which makes it a good place to deploy a persistent service.

Deployed on Heroku, [here](https://mefit-auth.herokuapp.com/auth/).


Created by Ayoub Auolad ali, Lev Nagornov, Rik Dortmans.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

The app is already [running](https://mefit-auth.herokuapp.com/auth/) on Heroku. Follow the next steps if you want to set up your own instance.

1. Clone the repository using:

```
git clone git@gitlab.com:lnagornov/mefit-auth.git
```

2. Download and install: 
* [Docker](https://docs.docker.com/get-docker/)
* [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

3. You need a Heroku account to deploy and run your apps, please register your account [here](https://signup.heroku.com) or sign in to existing one to continue.

4. Create a new heroku app. 
  * Go to [Heroku app dashboard](https://dashboard.heroku.com/apps), click **New**, then click **Create new app**
  * Give your app a name in the **App name** field. Please, remember this name, you will need it further.
  * Choose a region of server location.
  * Press **Create app** to finish app creation.

5. Using terminal navigate to project directory with **Dockerfile**.

6. Next steps have been taken from [Heroku official guide "Container Registry & Runtime (Docker Deploys")](https://devcenter.heroku.com/articles/container-registry-and-runtime#pushing-an-existing-image).

7. You must be logged in to the Heroku container registry use the following command if you are not already logged in as per the official documentation mentioned above.

```
heroku container:login
```

8. Heroku provides a single command to build and push the image. Run this command:

```
heroku container:push -a <you-heroku-app-name> web
```

9. Open a page of your created app on Heroku.
  You can use this link, just change the app new in the link:
  https://dashboard.heroku.com/apps/ **you-heroku-app-name**

10. Check that your app have installed add-on of **Heroku Postgres**. Open **Resources** tab and if your add-ons list doesn't have it, manually add with **Find more add-ons** button

11. Once you have **Heroku Postgres** installed as your add-on, you need to get connection URI of it. 
  * Open the add-on by clicking on it.
  * Open **Settings** tab of the add-on.
  * Click **View credentials** in ADMINISTRATION / Database Credentials list.
  * Find **URI** string, it should has this structure 
  ```
  postgres://username:password@host:port/database
  ```

  Copy your uri string, you will need it in the next step.
 
12. Declare some additional environment variables for Keycloak on **Settings** tab of your app. Feel free to change the values of the **KEYCLOAK_USER**, **KEYCLOAK_PASSWORD**, **PORT** variables.

```
KEYCLOAK_USER=admin
KEYCLOAK_PASSWORD=admin
PROXY_ADDRESS_FORWARDING=true
DATABASE_URL=paste here your copied uri string
PORT=8080
```

13. Now you can start the application by running this command, and then browsing to our running Keycloak instance:

```
heroku container:release -a <you-heroku-app-name> web
```

## Usage

1. Open your [Heroku app dashboard](https://dashboard.heroku.com/apps), click on your app and then click **Open app** to open tab with your app running.

2. Then you can start following [Keycloak official "Getting started guide"](https://www.keycloak.org/getting-started/getting-started-docker) to set up your Keycloak instance.

## Maintainers

[@AyoubAuolad](https://gitlab.com/AyoubAuolad)
[@lnagornov](https://gitlab.com/lnagornov)
[@rikdortmans](https://gitlab.com/rikdortmans)

## Contributing

PRs accepted.

Small note: If editing the Readme, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[MIT © Ayoub Auolad ali, Lev Nagornov, Rik Dortmans.](../LICENSE)
